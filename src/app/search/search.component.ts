import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color, isAndroid } from "@nativescript/core";
import { NoticiasService } from "./../domain/noticias.service";
// import { isAndroid } from "tns-core-modules/platform";
// import { View } from "tns-core-modules/ui/core/view";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
})
export class SearchComponent implements OnInit {
    androidValue = "";
    resultados: Array<string>;
    @ViewChild("layout") layout: ElementRef;

    constructor(public noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        if (isAndroid) this.androidValue = "Is Android";
        this.noticias.agregar("hola");
        this.noticias.agregar("hola 2");
        this.noticias.agregar("hola 3");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(ev: any) {
        console.dir(ev);
    }

    searchNow(s: string) {
        this.resultados = this.noticias.buscar().filter(x => x.indexOf(s) >= 0);

        const layout = this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        })).then(() => layout.animate({
            opacity: 0,
            duration: 300,
            delay: 150
        })).then(() => layout.animate({
            opacity: 1,
            duration: 300,
            delay: 150
        }));
    }
}
