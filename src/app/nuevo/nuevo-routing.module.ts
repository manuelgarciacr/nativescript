import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { Nuevo1Component } from "./nuevo1/nuevo1.component";

const routes: Routes = [
    { path: "", component: Nuevo1Component },
    { path: "nuevo2", component: Nuevo1Component }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class NuevoRoutingModule {}
