import { Component } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { RouterExtensions } from "@nativescript/angular";


@Component({
    selector: "Nuevo1",
    templateUrl: "./nuevo1.component.html",
    styleUrls: ["./nuevo1.component.css"]
})
export class Nuevo1Component {
    constructor(private routerExtensions: RouterExtensions) {}

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
    onTap(): void {
        this.routerExtensions.navigate(["../nuevo2"], {
            transition: {
                name: "fade"
            }
        });
    }
}
