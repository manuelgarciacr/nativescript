import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from '@nativescript/angular';
// import { CommonModule } from '@angular/common';

import { NuevoRoutingModule } from "./nuevo-routing.module";
import { Nuevo1Component } from "./nuevo1/nuevo1.component";
import { Nuevo2Component } from "./nuevo2/nuevo2.component";

@NgModule({
    declarations: [
        Nuevo1Component,
        Nuevo2Component
    ],
    imports: [
//    CommonModule,
        NativeScriptCommonModule,
        NuevoRoutingModule
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class NuevoModule {}
