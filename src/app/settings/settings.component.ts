import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
// import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toasts";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn: () => void) {setTimeout(fn, 1000); }

    ngOnInit(): void {
/*         this.doLater(() => {
            dialogs.action("Mensaje", "Cancelar!", ["Opción 1", "Opción 2"])
                .then(result => {
                    console.log("Resultado", result);
                    if (result === "Opción 1")
                        this.doLater(() => {
                            dialogs.alert({
                                title: "Título 1",
                                message: "Msg 1",
                                okButtonText: "Btn 1"
                            }).then(() => console.log("Cerrado 1"));
                        });
                    else
                        this.doLater(() => {
                            dialogs.alert({
                                title: "Título 2",
                                message: "Msg 2",
                                okButtonText: "Btn 2"
                            }).then(() => console.log("Cerrado 2"));
                        });
                });
        });
 */
        const toastOptions: Toast.ToastOptions = {text: "Hellow World", duration: Toast.DURATION.SHORT};
        this.doLater(() => Toast.show(toastOptions));
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
