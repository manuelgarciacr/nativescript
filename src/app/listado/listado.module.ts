import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import {
    NativeScriptCommonModule,
    NativeScriptFormsModule,
} from "@nativescript/angular";

import { ListadoRoutingModule } from "./listado-routing.module";
import { ListadoComponent } from "./listado/listado.component";
import { DetalleComponent } from "./detalle/detalle.component";
import { EditFormComponent } from "./detalle/edit-form.component";
import { MinLenDirective } from "../directives/min-len.directive";

@NgModule({
    declarations: [
        ListadoComponent,
        DetalleComponent,
        EditFormComponent,
        MinLenDirective,
    ],
    imports: [
        NativeScriptCommonModule,
        ListadoRoutingModule,
        NativeScriptFormsModule,
    ],
    schemas: [NO_ERRORS_SCHEMA],
})
export class ListadoModule {}
