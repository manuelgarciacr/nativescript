import { Component } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, ItemEventData } from "@nativescript/core";
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "Listado",
    templateUrl: "./listado.component.html",
})
export class ListadoComponent {
    items = ["Item1", "Item2", "Item3"];

    constructor(private routerExtensions: RouterExtensions) { }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
    onItemTap(ev: ItemEventData): void {
        this.routerExtensions.navigate(["listado/detalle"], {
            queryParams: {
                item: this.items[ev.index],
            },
            transition: {
                name: "fade",
            },
        });
    }
}
