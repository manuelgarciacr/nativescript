import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { ListadoComponent } from "./listado/listado.component";
import { DetalleComponent } from "./detalle/detalle.component";

const routes: Routes = [
    { path: "", redirectTo: "listado", pathMatch: "full" },
    { path: "listado", component: ListadoComponent },
    { path: "detalle", component: DetalleComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ListadoRoutingModule {}
