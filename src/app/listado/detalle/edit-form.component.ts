import {
    Component,
    Output,
    EventEmitter,
    Input
} from "@angular/core";

@Component({
    selector: "EditForm",
    styles: [
        `
            TextField {
                background-color: #f0f0f0;
                font-size: 24px;
                border: 8px solid black;
            }
            Button {
                background-color: lightblue;
                color: black;
                font-size: 25px;
                border: 1px solid blue;
                border-radius: 5px;
            }
            Label {
                font-size: 20px;
                color: red;
            }
        `,
    ],
    template: ` <FlexboxLayout flexDirection="row">
            <TextField
                #texto="ngModel"
                [(ngModel)]="textFieldValue"
                hint="Enter text..."
                required
                [minlen]="4"
            ></TextField>
            <Label *ngIf="texto.hasError('required')" text="*"></Label>
            <Label *ngIf="!texto.hasError('required') && texto.hasError('minlen')" text="4+"></Label>
        </FlexboxLayout>
        <Button
            class="btn primary"
            text="Actualizar"
            (tap)="onButtonTap()"
            *ngIf="texto.valid"
        ></Button>`,
    })
export class EditFormComponent {
    @Input() textFieldValue: string = "";
    @Output() actualizar: EventEmitter<string> = new EventEmitter();

    onButtonTap(): void {
        console.log(this.textFieldValue);
        // if (this.textFieldValue.length > 2) {
        this.actualizar.emit(this.textFieldValue);
        // }
    }
}
