import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { registerElement } from "@nativescript/angular";
import { ItemEventData, GestureEventData, ViewBase } from "@nativescript/core";
import { Page } from "tns-core-modules/ui/page";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toasts";

import { PullToRefresh } from "@nstudio/nativescript-pulltorefresh";
registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

@Component({
    selector: "Detalle",
    templateUrl: "./detalle.component.html",
    styles: [`
        .icon-cls {
            height: 100px;
            opacity: .2;
            text-align: center;
            margin-top: 20px;
            font-size: 30px;
        }
`
    ]
})
export class DetalleComponent {
    item: string;
    subItems = [];
    refreshing = false;
    editing = false;

    view: any;
    text: string;

    constructor(private route: ActivatedRoute, private page: Page) {
        this.route.queryParams.subscribe((params) => {
            this.item = params.item;
        });
        this.refreshList({refreshing: false});
    }

    loadItems() {
        return new Promise((resolve, reject) => {
            try {
                setTimeout(() => {
                    for (let i = 0; i < 6; i++) {
                        const item = Math.floor(Math.random() * 100);
                        this.subItems.unshift(item);
                    }
                    resolve("great success");
                }, this.subItems.length ? 4000 : 0);
            } catch (ex) {
                reject(ex);
            }
        });
    }
    refreshList(args) {
        const pullRefresh = args.object as PullToRefresh;
        this.refreshing = true;
        this.loadItems().then(
            (response) => {
                console.log(response);
                pullRefresh.refreshing = false;
            },
            (err) => {
                pullRefresh.refreshing = false;
                alert(err);
            }
        ).finally(() => {this.refreshing = false; });
    }
    onDelete(ev: GestureEventData) {
        if (this.refreshing || this.editing)
            return;
        const id = parseInt(ev.view.id.slice(1), 10);
        const el = this.subItems.splice(id, 1);
        dialogs.alert({
            title: "Eliminar Entrada",
            message: "Eliminado Comentario " + el[0],
            okButtonText: "Ok"
        });
    }
    onVoteUp(ev: GestureEventData) {
        if (this.refreshing || this.editing)
            return;
        const id = parseInt(ev.view.id.slice(2), 10);
        const view = this.page.getViewById("P" + id);
        let val = parseInt(view.get("text").slice(10), 10);
        dialogs.action("Votar", "Cancelar!", ["+1", "+2", "+3"])
            .then(result => {
                if (result === "+1")
                    val++;
                else if (result === "+2")
                    val = val + 2;
                else if (result === "+3")
                    val = val + 3;
                if (result !== "Cancelar!") {
                    view.setProperty("text", "Puntuaje: " + val);
                    const toastOptions: Toast.ToastOptions = {
                        text: "Nuevo puntuaje: " + val,
                        duration: Toast.DURATION.SHORT
                    };
                    Toast.show(toastOptions);
                }
            });
    }
    onVoteDown(ev: GestureEventData) {
        if (this.refreshing || this.editing)
            return;
        const id = parseInt(ev.view.id.slice(2), 10);
        const view = this.page.getViewById("P" + id);
        let val = parseInt(view.get("text").slice(10), 10);
        dialogs.action("Votar", "Cancelar!", ["-1", "-2", "-3"])
            .then(result => {
                if (result === "-1")
                    val--;
                else if (result === "-2")
                    val = val - 2;
                else if (result === "-3")
                    val = val - 3;
                if (result !== "Cancelar!") {
                    view.setProperty("text", "Puntuaje: " + val);
                    const toastOptions: Toast.ToastOptions = {
                        text: "Nuevo puntuaje: " + val,
                        duration: Toast.DURATION.SHORT
                    };
                    Toast.show(toastOptions);
                }
            });
    }
    onEdit(ev: GestureEventData) {
        if (this.refreshing || this.editing)
            return;
        const id = parseInt(ev.view.id.slice(2), 10);
        this.view = this.page.getViewById("COM" + id);
        this.text = this.view.get("text");
        this.editing = true;
    }
    actualizar(s: string) {
        this.view.setProperty("text", s);
        this.editing = false;
    }
    onItemTap(ev: ItemEventData) {
        console.dir("OIT", ev.index);
    }
}
