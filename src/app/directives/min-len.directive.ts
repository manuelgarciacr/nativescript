import { Directive, Input } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";

@Directive({
    selector: "[minlen]",
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: MinLenDirective,
            multi: true,
        },
    ],
})
// tslint:disable-next-line: directive-class-suffix
export class MinLenDirective implements Validator {
    @Input() minlen: string;

    validate(control: AbstractControl): { [key: string]: any } {
        return !control.value || control.value.length >= (this.minlen || 2)
            ? null
            : { minlen: true };
    }
}
